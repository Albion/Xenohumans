﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using Verse.AI;
using HarmonyLib;

namespace Albion
{
    [StaticConstructorOnStartup]
    public static class HarmonyPatches
    {
        static HarmonyPatches()
        {
            Harmony harmony = new Harmony("Albion.Xenohumans");
            Type typeFromHandle = typeof(HarmonyPatches);
            harmony.Patch(AccessTools.Method(typeof(PawnGenerator), "GeneratePawn", new Type[]
            {
                typeof(PawnGenerationRequest)
            }, null), null, new HarmonyMethod(typeFromHandle, "Post_GeneratePawn", null), null, null);
        }

        public static void Post_GeneratePawn(PawnGenerationRequest request, ref Pawn __result)
        {
            Pawn pawn = __result;
            HediffGiverSetDef hediffGiverSetDef;
            if (pawn == null)
            {
                hediffGiverSetDef = null;
            }
            else
            {
                ThingDef def = pawn.def;
                if (def == null)
                {
                    hediffGiverSetDef = null;
                }
                else
                {
                    RaceProperties race = def.race;
                    if (race == null)
                    {
                        hediffGiverSetDef = null;
                    }
                    else
                    {
                        List<HediffGiverSetDef> hediffGiverSets = race.hediffGiverSets;
                        if (hediffGiverSets == null)
                        {
                            hediffGiverSetDef = null;
                        }
                        else
                        {
                            hediffGiverSetDef = hediffGiverSets.FirstOrDefault((HediffGiverSetDef x) => GenCollection.Any<HediffGiver>(x.hediffGivers, (HediffGiver y) => y is HediffGiver_Initial));
                        }
                    }
                }
            }
            HediffGiverSetDef hediffGiverSetDef2 = hediffGiverSetDef;
            bool flag = hediffGiverSetDef2 == null;
            if (!flag)
            {
                HediffGiver_Initial hediffGiver_Initial = hediffGiverSetDef2.hediffGivers.FirstOrDefault((HediffGiver x) => x is HediffGiver_Initial) as HediffGiver_Initial;
                bool flag2 = hediffGiver_Initial != null;
                if (flag2)
                {
                    hediffGiver_Initial.GiveHediff(__result);
                }
            }
        }

        public static Dictionary<Thing, int> AlternatingFireTracker = new Dictionary<Thing, int>();

        public static bool StopPreApplyDamageCheck;

        public static int? tempDamageAmount = null;

        public static int? tempDamageAbsorbed = null;
    }

    public class HediffGiver_Initial : HediffGiver
    {
        public void GiveHediff(Pawn pawn)
        {
            bool flag = this.chance < Rand.Range(0f, 100f);
            if (!flag)
            {
                bool flag2 = pawn.gender == Gender.Male && this.maleCommonality < Rand.Range(0f, 100f);
                if (!flag2)
                {
                    bool flag3 = pawn.gender == Gender.Female && this.femaleCommonality < Rand.Range(0f, 100f);
                    if (!flag3)
                    {
                        bool flag4 = this.expandedDef != null;
                        if (flag4)
                        {
                            HealthUtility.AdjustSeverity(pawn, this.expandedDef, 1f);
                        }
                        else
                        {
                            HealthUtility.AdjustSeverity(pawn, this.hediff, 1f);
                        }
                    }
                }
            }
        }

        public float chance = 100f;

        public float maleCommonality = 100f;

        public float femaleCommonality = 100f;

        public HediffInitialDef expandedDef;
    }

    public class HediffInitialDef : HediffDef
    {
        public bool showDescription;

        public string preListText;

        public string postListText;
    }
}
